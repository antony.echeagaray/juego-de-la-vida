// Obtener elementos del DOM
const grid = document.getElementById('grid'); // La cuadrícula donde se mostrarán las células
const startStopBtn = document.getElementById('startStop'); // Botón para iniciar/detener la simulación
const randomBtn = document.getElementById('random'); // Botón para generar un estado inicial aleatorio
const clearBtn = document.getElementById('clear'); // Botón para limpiar la cuadrícula
const gridSize = 60;  // Tamaño de la cuadrícula (60x60)
const generationsDisplay = document.getElementById('generations'); // Elemento para mostrar el número de generaciones
const aliveCellsDisplay = document.getElementById('aliveCells');  // Elemento para mostrar el número de células vivas
const deadCellsDisplay = document.getElementById('deadCells'); // Elemento para mostrar el número de células muertas
let isRunning = false; // Indica si la simulación está en ejecución
let interval;  // Para controlar el intervalo de actualización
let generations = 0; // Contador de generaciones
  

// Inicializar la cuadrícula
for (let i = 0; i < gridSize * gridSize; i++) {  // Este bucle se ejecuta gridSize*gridSize veces para crear cada celda en la cuadrícula.
    const cell = document.createElement('div');  // Crea un nuevo elemento div
    cell.classList.add('cell'); // Añade la clase 'cell' para aplicar estilos predefinidos
    cell.addEventListener('click', () => toggleCell(i)); // Añade un evento de click a la celda que alterna su estado
    grid.appendChild(cell);  // Agrega la celda recién creada como un hijo del elemento de cuadrícula
}

// Función para cambiar el estado de una celda
function toggleCell(index) {
    const cells = Array.from(grid.children); // Convierte los hijos de 'grid' en un array
    cells[index].classList.toggle('alive'); // Alterna la clase 'alive' para cambiar el estado de la celda
}

// Función para actualizar la cuadrícula en cada generación
function updateGrid() {
    const cells = Array.from(grid.children);
    let aliveCount = 0;  // Contador para las células vivas
    // Determinar el nuevo estado de cada celda basado en las reglas del juego
    const newGrid = cells.map((cell, index) => {
        const isAlive = cell.classList.contains('alive');
        const neighbors = countNeighbors(index);
        // Cuenta los vecinos vivos
        if (isAlive && (neighbors < 2 || neighbors > 3)) return false; // Una célula viva con dos o tres células vecinas vivas permanece viva.
        if (!isAlive && neighbors === 3) return true; //Una célula muerta con exactamente tres células vecinas vivas se convierte en una célula viva.
        return isAlive;
    });
     // Actualizar la cuadrícula basada en la constante 'newGrid'
    newGrid.forEach((state, index) => {
        cells[index].classList.toggle('alive', state);
    });
    // Contar células vivas y muertas
    newGrid.forEach(state => {
        if (state) aliveCount++; //Aumenta el contador de las celulas vivas
    });

      // Incrementar y actualizar estadísticas solo si el juego está en ejecución
    if (isRunning) {
        generations++; //Aumenta el contador de las generaciones
        generationsDisplay.textContent = generations; //Coloca la información de las generaciones en el DOM
    }

    aliveCellsDisplay.textContent = aliveCount;
    deadCellsDisplay.textContent = gridSize * gridSize - aliveCount;
}

// Función para contar los vecinos vivos de una celda
function countNeighbors(index) {
    // Calcula la fila y la columna de la celda actual basándose en el índice
    const row = Math.floor(index / gridSize);  // Determina la fila dividiendo el índice por el tamaño de la cuadrícula
    const col = index % gridSize; // Determina la columna usando el módulo del índice por el tamaño de la cuadrícula
    let count = 0; // Inicializa el contador de vecinos vivos

    // Itera sobre todas las celdas vecinas inmediatas
    for (let i = -1; i <= 1; i++) {
        for (let j = -1; j <= 1; j++) {
            if (i === 0 && j === 0) continue; // Ignora la celda actual (la celda en cuestión no es su propia vecina)
            const newRow = row + i; // Calcula la fila de la celda vecina
            const newCol = col + j; // Calcula la columna de la celda vecina
             // Verifica si la celda vecina está dentro de los límites de la cuadrícula
            if (newRow >= 0 && newRow < gridSize && newCol >= 0 && newCol < gridSize) {
                const neighborIndex = newRow * gridSize + newCol; // Calcula el índice de la celda vecina
                // Si la celda vecina está viva, incrementa el contador
                if (grid.children[neighborIndex].classList.contains('alive')) count++;
            }
        }
    }

    return count; // Devuelve el número total de vecinos vivos
}

// Botones de control
randomBtn.addEventListener('click', () => {
    // Generar un estado inicial aleatorio
    Array.from(grid.children).forEach(cell => {// Itera sobre cada celda de la cuadrícula
        // Genera un estado booleano aleatorio para cada celda
        const randomState = Math.random() < 0.5; // Hay 50% de probabilidad de que la celda esté viva
        // Si randomState es verdadero, agrega la clase 'alive'; si es falso, la remueve
        cell.classList.toggle('alive', randomState);
    });
    // Calcula el número de células vivas después de la aleatorización
    const aliveCount = Array.from(grid.children).filter(cell => cell.classList.contains('alive')).length;
     // Reinicia el contador de generaciones y actualiza su visualización
    generationsDisplay.textContent = 0;
    // Actualiza la visualización del número de células vivas
    aliveCellsDisplay.textContent = aliveCount;
    // Calcula y actualiza la visualización del número de células muertas
    deadCellsDisplay.textContent = gridSize * gridSize - aliveCount;
});

startStopBtn.addEventListener('click', () => {
    // Iniciar o detener la simulación
    isRunning = !isRunning;
    if (isRunning) {
        interval = setInterval(updateGrid, 10);
    } else {
        clearInterval(interval);
    }
});

//Limpiar Cuadricula y reiniciar las estadisticas
clearBtn.addEventListener('click', () => {
    Array.from(grid.children).forEach(cell => cell.classList.remove('alive')); //Se elimina la clase alive de cada celda
    // Detener el intervalo de actualización
    clearInterval(interval);
    isRunning = false;
    // Reiniciar estadísticas
    generations = 0;
    generationsDisplay.textContent = 0;
    aliveCellsDisplay.textContent = 0;
    deadCellsDisplay.textContent = gridSize * gridSize;
});
